/**
 * @file
 * The dialog for CKEditor
 *
 * Loaded by the CKEditor plugin system
 */


(function ($) {
  CKEDITOR.dialog.add('collapseDialog', function (editor) {
    return {
      title:Drupal.t('Collapsible Text'),
      minWidth:400,
      minHeight:200,
      contents:[
        {
          id:'tab-basic',
          label:Drupal.t('Basic Settings'),
          elements:[
            {
              type:'text',
              id:'title',
              label:Drupal.t('Collapsible Text Title'),
              validate:CKEDITOR.dialog.validate.notEmpty(Drupal.t("Title field cannot be empty.")),
              setup:function (widget) {
                this.setValue(widget.data.title);
              },
              commit:function (widget) {
                widget.setData('title', this.getValue());
              },
            },
            {
              type:'checkbox',
              id:'collapsed',
              label:Drupal.t('Collapsed by default'),
              setup:function (widget) {
                this.setValue(widget.data.collapsed);
              },
              commit: function(widget) {
                widget.setData('collapsed',this.getValue());
              },
            }
          ],
        },
      ],
    };
  });
})(jQuery);