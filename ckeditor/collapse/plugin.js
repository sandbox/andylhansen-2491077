/**
 * @file
 * The plugin configuration for CKEditor
 *
 */

(function ($) {
  CKEDITOR.plugins.add('collapse', {
    requires:'widget',
    icons:'collapse',
    
    init:function (editor) {

      CKEDITOR.dialog.add('collapseDialog', this.path + 'dialogs/collapse.js' );  

      editor.widgets.add('collapse', {
        template:
          '<div class="collapse-text collapsed">' +
            '<div class="collapse-text-title">Title</div>' +
            '<div class="collapse-text-wrapper"><p>Text Goes Here</p></div>' +
          '</div>',
        
        editables:{
          content:{
            selector:'.collapse-text-wrapper',
          },
        },
        
        allowedContent:'div(collapse-text,collapsed,editor-hide,collapse-text-wrapper,collapse-text-title)',
        
        requiredContent:'div(collapse-text,collapse-text-title,collapse-text-wrapper,editor-hide); div(collapsed)',
        
        upcast:function (element) {
          if (element.name!='div' || !element.hasClass('collapse-text') || element.children.length!=2) {
            return false;
		  }
          
          var title = element.children[0];
          var text = element.children[1];
          
          if (!title.name || !text.name) { return false; }
          if (title.name != 'div' || text.name != 'div') {return false; }
          if (!title.hasClass('collapse-text-title')) { return false; }
          if (!text.hasClass('collapse-text-wrapper')) { return false; }

          return true;
        },
        
        dialog:'collapseDialog',
        
        init:function () {
          var element = this.element;
          var title = element.find('.collapse-text-title').getItem(0);
          
          if (title) {
            var titleText = title.getText();
		  }
          else {
            var titleText = 'Title';
		  }
          
          this.setData('title', titleText);
          this.setData('collapsed', element.hasClass('collapsed'));
          
          if (title) {
            title.on('click', function (e) {
              if (e.data.getKey()==1) {
                if (element.hasClass('editor-hide')) {
                  element.removeClass('editor-hide');
			    }
                else {
                  element.addClass('editor-hide');
			    }
              }
            });
          }

        },
        
        data:function () {
          this.element.find('.collapse-text-title').getItem(0).setText(this.data.title);
          
          if (this.data.collapsed) {
            this.element.addClass('collapsed');
		  }
          else {
            this.element.removeClass('collapsed');
		  }
        },
        
        
      });
      
      if (editor.contextMenu) {
        editor.addMenuGroup('collapseGroup');
        editor.addMenuItem('collapseItem', {
          label: 'Collapsible',
          icon: this.path + 'icons/collapse.png',
          command: 'collapse',
          group:'collapseGroup',
        });

        editor.contextMenu.addListener(function(element) {
            if (element.hasClass('cke_widget_wrapper') && element.getChild(0).hasClass('collapse-text')) {
                return { collapseItem: CKEDITOR.TRISTATE_OFF };
            }
        });
      }

      editor.ui.addButton('collapse_button', {
        label:'Insert Collapsible Region',
        command:'collapse',
        toolbar:'insert',
        icon:this.path + 'icons/collapse.png',
      });
      
      editor.addContentsCss(this.path + 'styles/collapse.css');
        
    }
    
  });
})(jQuery);