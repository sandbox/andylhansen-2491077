/**
 * @file
 * Handles the collapsing/expanding when viewed on a rendered page
 *
 */

(function ($) {
  "use strict";

  /**
   * Attaches the collapse behavior to the appropriate elements
   *
   * @property {function} attach
   *   Sets up event handlers and animation
   */	
  Drupal.behaviors.collapseWysiwyg = {
    attach: function (context, settings) {
	  //create empty div to calculate width
  	  $('.collapse-text',context).once('collapse-width-processed',function(){
		var empty = $('<div class="collapse-text-width"></div>')[0];
		this.appendChild(empty);
  	  });
		
      $('.collapse-text-title', context).once('collapse-text-processed', function () {
        var text = $(this).text();
        
        if ($(this).parent().hasClass('collapsed')) {
		  $(this).next('.collapse-text-wrapper').css('display','none');
	    }
      
        $(this).html('<a href="#">' + Drupal.t('@t', {'@t':text}) + '</a>');
		
        $(this).find('a').click(function (e) {
          e.preventDefault();
          
          var $element = $(this).parent().parent();
		  var $width = $element.children('.collapse-text-width');
		  var $wrapper = $element.children('.collapse-text-wrapper');
        
          $element.toggleClass('collapsed');
		  var originalWidth = $wrapper.css('width');
		  //set fixed width to prevent jumping in the slide animation
		  $wrapper.css('width',$width.width() + 'px');

		  //remove the width attribute to prevent problems with fluid width layouts
          var finish = function () {
          	$wrapper.removeAttr('style');
          }

          if ($element.hasClass('collapsed')) {
            $wrapper.slideUp(400);
		  }
          else {
            $wrapper.slideDown(400,finish);
		  }
        });
      });
    }
  };
  
})(jQuery);