This module extends the wysiwyg module to provide a plugin to create collapsible text regions that can be edited directly in the wysiwyg editor.

Currently only supports CKEditor.

You must have the CKEditor 'widget' plugin and its dependencies installed inside the libraries/ckeditor/plugins folder.

Enable the module and enable the 'Collapse' plugin on the wysiwyg profile configuration page.